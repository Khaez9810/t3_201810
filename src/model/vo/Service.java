package model.vo;

import sun.print.resources.serviceui;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	//atributos objeto servicio

	private String tripID;

	private String taxiID;

	private int tripSeconds;

	private double tripMiles;

	private double tripTotal;

	private int comunityArea;
	
	private String tripStartTime;



	public Service (String TripID,String TaxiD, int tripSeconds, double tripMiles, double tripTotal, int comunityArea, String tripStartTime){
		this.tripID = TripID;
		this.taxiID = TaxiD;
		this.tripSeconds = tripSeconds;
		this.tripMiles = tripMiles;
		this.tripTotal = tripTotal;
		this.comunityArea = comunityArea;
		this.tripStartTime = tripStartTime;
		
		
	}

	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return tripID;
	}	

	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxiID;
	}	

	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return tripSeconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return tripMiles;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return tripTotal;
	}


	/**
	 * @return startime - Begin Time Stamp
	 */
	public String getTripStartTime() {
		// TODO Auto-generated method stub
		return tripStartTime;
	}

	/**
	 * Retorna 1 si es mayor
	 * Retorna 0 si es igual
	 * Retorna -1 si es menor
	 */
	@Override
	public int compareTo(Service o) {
		if(tripStartTime.equals("N/A")){
			return 1;
		}
		else if(tripStartTime.compareTo(o.getTripStartTime())>0) return 1;
		else if(tripStartTime.compareTo(o.getTripStartTime())<0) return -1;
		else return 0;

		}
	}

