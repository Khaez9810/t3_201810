package model.vo;
import com.sun.org.apache.regexp.internal.recompile;

import model.data_structures.*;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	private String taxiId;
	private String company;
	private Stack<Service> inverseOrderServices;
	private Queue<Service> orderServices;
	
	/**
	 * MEME REVIEW
	 */
	public Taxi(String taxiId, String company) {
		this.taxiId=taxiId;
		this.company=company;
		inverseOrderServices = new Stack<Service>();
		orderServices = new Queue<Service>();
		
	}
	
	/**
	 * MEME REVIEW
	 */
	public void registerService(Service ser) throws Exception{
		inverseOrderServices.push(ser);
		orderServices.enqueue(ser);
	}
	
	/**
	 * MEME REVIEW
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxiId;
	}

	/**
	 * MEME REVIEW
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return company;
	}
	
	/**
	 * MEME REVIEW
	 */
	public Stack getInverseOrderServices() {
		return inverseOrderServices;
		
	}
	
	/**
	 * MEME REVIEW
	 */
	public Queue getOrderedServices() {
		return orderServices;
	}
	
	@Override
	/**
	 * MEME REVIEW
	 */
	public int compareTo(Taxi o) {
		return 0;
	}	
}
