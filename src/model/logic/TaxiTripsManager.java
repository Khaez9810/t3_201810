package model.logic;

import java.io.FileReader;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.Taxi;
import model.vo.Service;

public class TaxiTripsManager implements ITaxiTripsManager {

	private LinkedList<Taxi> ListaDeTaxis;
	private Taxi taxi;
	private String file;
	
	/**
	 * MEME REVIEW
	 */
	public void loadServices(String serviceFile, String taxiId) {
		if(serviceFile != file){
			taxi = null;
			file = serviceFile;
			JsonParser parser = new JsonParser();
			ListaDeTaxis = new LinkedList<Taxi>();
			String time_start_stamp="",taxi_id="",tripid="",company="";
			double trip_miles=0,trip_total=0; int trip_seconds=0,dropof_comunity_area=0;
			try {
				JsonArray array = (JsonArray) parser.parse(new FileReader(serviceFile));
				int size = array.size();
				for (int i = 0; i < size; i++) {
					JsonObject ob = (JsonObject) array.get(i);


					//Si el atributo no existe se declara como N/A o 0.
					company					= ob.get("company")!=null?ob.get("company").getAsString():"N/A";
					time_start_stamp 		= ob.get("trip_start_timestamp")!=null?ob.get("trip_start_timestamp").getAsString():"N/A";
					dropof_comunity_area 	= ob.get("dropoff_community_area") != null?ob.get("dropoff_community_area").getAsInt():0;
					taxi_id 				= ob.get("taxi_id")!=null?ob.get("taxi_id").getAsString():"N/A";
					trip_miles 				= ob.get("trip_miles")!=null?ob.get("trip_miles").getAsDouble():0;
					trip_total 				= ob.get("trip_total")!=null?ob.get("trip_total").getAsDouble():0;
					trip_seconds 			= ob.get("trip_seconds")!=null?ob.get("trip_seconds").getAsInt():0;
					tripid 					= ob.get("trip_id")!=null?ob.get("trip_id").getAsString():"N/A";
					System.out.println(taxi_id);
					if(encontrarTaxi(taxi_id)==null) {
						Taxi actual = new Taxi(taxi_id, company);
						Service ser = new Service(tripid, taxi_id, trip_seconds, trip_miles, trip_total, dropof_comunity_area, time_start_stamp);
						actual.registerService(ser);ListaDeTaxis.add(actual);
						if(taxi_id.equals(taxiId))
							taxi=actual;
					}
					else{
						Service ser = new Service(tripid, taxi_id, trip_seconds, trip_miles, trip_total, dropof_comunity_area, time_start_stamp);
						Taxi bla = encontrarTaxi(taxiId); bla.registerService(ser);
						if(taxi_id.equals(taxiId))
							taxi=bla;
					}
					
				}
			}catch (Exception e) {
				System.out.println("Add data to structure error, plz ignore");
			}
		}
		else if(taxiId != taxi.getTaxiId() && serviceFile == file)
			taxi = encontrarTaxi(taxiId);
	}
	
	/**
	 * MEME REVIEW
	 */
	public Taxi encontrarTaxi(String taxiId){
		for (int i = 0; i < ListaDeTaxis.getSize(); i++) 
			if(ListaDeTaxis.getAt(i).getTaxiId().equals(taxiId))
				return ListaDeTaxis.getAt(i);
		return null;
	}
	
	/**
	 * MEME REVIEW
	 */
	@Override
	public int [] servicesInInverseOrder() {
		Stack pila = taxi.getInverseOrderServices();
		int [] resultado = new int[2];
		resultado[0]=0;resultado[1]=0;
		Service aux1 =(Service) pila.pop();
		while(!pila.isEmpty()){
			Service aux2 = (Service) pila.pop();
			if(aux2 != null){
				if(aux1.compareTo(aux2)<=0) resultado[0]++;
				else resultado[1]++;
			}
			aux1=aux2;
		}
		System.out.println("Inside servicesInInverseOrder");
		
		return resultado;
	}

	/**
	 * MEME REVIEW
	 */
	@Override
	public int [] servicesInOrder() {
		Taxi buscado = ListaDeTaxis.get(taxi);
		Queue cola = buscado.getOrderedServices();
		int [] resultado = new int[2];
		resultado[0]=0;resultado[1]=0;
		Service aux1 = (Service) cola.dequeue();
		while(!cola.isEmpty()){
			Service aux2 = (Service) cola.dequeue();
			if(aux2!=null){
				if(aux1.compareTo(aux2)>=0) resultado[0]++;
				else resultado[1]++;
			}
		}
		// TODO Auto-generated method stub
		System.out.println("Inside servicesInOrder");
		return resultado;
	}


}
