package model.data_structures;

public interface IStack<E> {

	/** Push a new element at the top of the stack 
	 * @throws Exception */
	public void push (E item) throws Exception;
	
	/** Pop the element at the top of the stack 
	 * @return the top element or null if it doesn't exist
	 * @throws Exception 
	 * */
	public E pop() throws Exception;
	
	/** Evaluate if the stack is empty
	 * @return true if the stack is empty. false in other case. 
	 */
	public boolean isEmpty();
}
