package model.data_structures;

public interface IQueue<E> {
	
	/** Enqueue a new element at the end of the queue 
	 * @throws Exception */
	public void enqueue(E item) throws Exception;
	
	/** Dequeue the "first" element in the queue
	 * @return "first" element or null if it doesn't exist
	 * @throws Exception 
	 */
	public E dequeue() throws Exception;
	
	/** Evaluate if the queue is empty. 
	 * @return true if the queue is empty. false in other case.
	 */
	public boolean isEmpty();
	
}
