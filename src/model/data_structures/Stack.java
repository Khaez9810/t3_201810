package model.data_structures;

public class Stack<T extends Comparable<T>> implements IStack<T>{

	private LinkedList<T> List;
	
	public Stack(){
		List = new LinkedList<T>();
		
	}
	
	public Node<T> getHead() {
		return List.getHead();
	}

	public int getSize() {
		return List.getSize();
	}

	@Override
	public void push(T item) throws Exception {
		boolean var = List.addAtHead(item);
		if(!var) 	throw new Exception("No se agrego a la pila");
		}
	
		// TODO Auto-generated method stub
		

	@Override
	public T pop(){
		T aux = getHead().getValue();
		boolean var = List.delete(aux);		
		if(var) return aux; else return null;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return getSize()==0;
	}
	

}
