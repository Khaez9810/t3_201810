package model.data_structures;

public class DoubleLinkedList<T> implements IList<T> {

	private Node<T> head;
	private Node<T> tail;
	private Iterador<T> current;
	private int size;

	public DoubleLinkedList() {
		head = tail = null;
		current = new Iterador<T>(null);
		size=0;
		// TODO Auto-generated constructor stub
	}

	public Node<T> getHead() {
		return head;
	}


	public Node<T> getTail() {
		return tail;
	}

	public Iterador<T> getIterador(){
		return current;
	}

	@Override
	public boolean add(T item) {
		Node<T> add = new Node<T>();
		add.setValue(item);
		boolean var=false;
		if(head==null) {
			head = tail = add;
			var=!var;
			size++;
			current.setIter(head);
		}else {
			add.setPrev(tail);
			tail.setNext(add);
			tail = add;
			var=!var;
			size++;
			current.setIter(tail);
		}
		return var;
	}

	public boolean addAtHead(T elem) {
		Node<T> add = new Node<T>(); add.setValue(elem);
		add.setNext(head);
		if(head!=null) head.setPrev(add);
		if(size==0)head = tail = add; else head = add;
		size++;
		return head==add; 
	}

	public boolean addAt(T elem, int index) {
		Node<T> add = new Node<T>(); add.setValue(elem);
		boolean var = false;
		if(index>size) {
			return false;
		}else {
			int aux = 0;
			current.setIter(head);
			while(aux< index-1 && current.hasNext()) {
				current.next();
				aux++;
			}
			Node<T> chain = current.getIter().getNext();
			current.getIter().setNext(add);
			add.setNext(chain);
			chain.setPrev(add);
			current.setIter(add);
			add.setPrev(current.getIter());
			current.setIter(add);
			size++;
			var=!var;	
		}
		return var;
	}

	@Override
	public boolean delete(T item) {
		boolean var = false;
		if(item==head.getValue()) {
			if(head.getNext()==null) {
				head = null;
			}else {
				head.getNext().setPrev(null);
				head = head.getNext();
				current.setIter(head);
			}
			size--;
			var =!var;
		}else{
			//Borra cualquier elemento
			current.setIter(head);
			boolean var2 = false;
			Node<T> aux,aux2;
			while(!var2 && current.hasNext()) {
				aux = current.getIter();
				if(aux.getNext().getValue()==item) 
					var2=!var2;
				else current.next();

			}
			aux = current.getIter();
			aux2 = aux.getNext().getNext();
			aux.setNext(aux2);
			aux2.setPrev(aux);
			size--;
			var=!var;
		}
		return var;
	}

	public boolean deleteAtHead() {
		if(head.getNext()!=null) {
		head = head.getNext();
		head.setPrev(null);
		size--;
		return true;
		}else {
			head = head.getNext();
			size--;
			return true;
		}
	}

	public boolean deleteAt(int index) {
		boolean rta = false;
		if(index>size) {
			return false;
		}else {
			int var = 0;
			current.setIter(head);
			Node<T> del = current.getIter().getNext();
			while(var< index-1 && current.hasNext()) {
				current.next();
				del = current.getIter().getNext();
				var++;
			}
			Node<T> aux = del.getNext();
			current.getIter().setNext(aux);
			aux.setPrev(current.getIter());
			size--;
			return !rta;
		}
	}

	@Override
	public T get(T item) {
		if(head.getValue()==item) {
			current.setIter(head);
			return head.getValue();
		}
		else if(tail.getValue()==item) {
			current.setIter(tail);
			return tail.getValue();
		}
		else{
			current.setIter(head);
			Node<T> aux = current.getIter();
			while(aux.getValue()!=item && current.hasNext()) {
				current.next();
				aux = current.getIter();
			}
			return (aux!=tail)?aux.getValue():null;
		}
	}


	@Override
	public T getAt(int pos) {
		if(pos>size) {
			return null;
		}else {
			int var = 0;
			current.setIter(head);
			while(var< pos && current.hasNext()) {
				current.next();
				var++;
			}
			return current.getValue();
		}
	}

	@Override
	public int getSize() {
		return size;
	}

	@Override
	public void listing() {
		Node<T> aux = current.getIter();
		current.prev(); Node<T> aux2 = current.getIter();
		aux.setPrev(null);
		aux2.setNext(null);
		tail.setNext(head);
		head.setPrev(tail);
		head = aux; tail = aux2;
		current.setIter(aux);

	}

	@Override
	public T getCurrent() {
		return current.getValue();

	}

	@Override
	public T next() {
		return current.next();
	}

	public T prev() {
		return current.prev();
	}

}
