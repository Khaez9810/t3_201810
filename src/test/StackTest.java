package test;
import junit.framework.*;
import model.data_structures.*;



public class StackTest extends TestCase{	
	private Stack<Integer> Pila;

	private void setupEscenario1() {
		Pila = new Stack<Integer>();
	}

	public void testStack() {
		setupEscenario1();
		assertTrue("No se inicializo la pila correctamente", Pila.isEmpty());
	}

	public void allTest() {
		try {
			Pila.push(1);
			Pila.push(3);
			Pila.push(4);
			Pila.push(10);
			Pila.push(15);
			assertSame("No se tiene el tamanio correctod de la pila", 5, Pila.getSize());
			assertSame("No es el elemento correcto en la cabeza", 15, Pila.getHead().getValue());
			
			
			int a = Pila.pop();
			assertSame("No se tiene el tamanio correctod de la pila", 4, Pila.getSize());
			assertSame("No es el elemento correcto en la cabeza", 10, Pila.getHead().getValue());
			assertSame("No se elimino el elemento correcto", 15, a);
			a = Pila.pop();
			assertSame("No se tiene el tamanio correctod de la pila", 3, Pila.getSize());
			assertSame("No es el elemento correcto en la cabeza", 4, Pila.getHead().getValue());
			assertSame("No se elimino el elemento correcto", 10, a);

		} catch (Exception e) {
			assertFalse(e.getMessage(),e!=null);
		}
	}


}
